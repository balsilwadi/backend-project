package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import utils.ConfigReader;
import utils.DBUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DBStepDef {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    @Given("User is able to connect to database")
    public void user_is_able_to_connect_to_database(){
        connection = DBUtil.createDBConnection();
    }

    @When("User inputs the correct quary")
    public void userInputsTheCorrectQuary() throws SQLException {
        statement = connection.createStatement();
        resultSet = statement.executeQuery(ConfigReader.getProperty("query"));
    }

    @Then("User should see the data displayed as below")
    public void userShouldSeeTheDataDisplayedAsBelow(DataTable dataTable) throws SQLException {
        List<String> expectedList = new ArrayList<>(dataTable.asList());
        List<String> actualList = new ArrayList();
        while (resultSet.next()) {
            actualList.add(resultSet.getString(1));
            actualList.add(resultSet.getString(2));

        }
        Assert.assertEquals(actualList, expectedList);
    }


}